package com.netcracker.ta.selenium;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * This class is for working with a browser through Selenium
 */
public class Selenium {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:/Web_Drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.google.ru/");
        WebElement search = driver.findElement(By.name("q"));
        search.sendKeys("Netcracker");
        search.submit();
        WebElement wiki = driver.findElement(By.partialLinkText("Википедия"));
        wiki.click();
        ArrayList <String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        try{
            WebElement link = driver.findElement(By.xpath("//table[@class = \"infobox vcard\"]//a[@href = \"http://www.netcracker.com/\"]"));
            link.click();
        } catch (NoSuchElementException e){
            System.out.println("ERROR");
        }
        //Second way
        /*WebElement link = driver.findElement(By.linkText("netcracker.com"));
        String str = "http://www.netcracker.com/";
        if(str.equals(link.getAttribute("href")))
            link.click();
        else System.out.println("ERROR");*/
    }
}

